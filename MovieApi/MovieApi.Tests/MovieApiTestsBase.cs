﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FluentAssertions;
using Moq;
using MovieApi.Infrastructure.Common;
using Newtonsoft.Json;
using Techniqly.Api.Providers.Common.Movie;
using Techniqly.RestEasy.Lib;

namespace Techniqly.Api.Movie.Tests
{
    internal class MovieApiTestsBase : IDisposable
    {
        protected static readonly string RootApiUri = "http://www.blah.com/api";
        protected readonly MovieFactory MovieFactory = new MovieFactory();
        private bool _disposed;
        private RequestFactory _requestFactory;

        protected JsonSerializerSettings JsonFormatter;
        protected HttpServer Server;

        protected Mock<IMovieProvider> MovieProviderMock { get; private set; }
        protected Mock<ILoggingSource> LoggingSourceMock { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void TestInitialize()
        {
            LoggingSourceMock = new Mock<ILoggingSource>();
            MovieProviderMock = new Mock<IMovieProvider>();
        }

        protected void ConfigureController(IMovieProvider movieProvider, ILoggingSource loggingSource)
        {
            var config = new HttpConfiguration();
            JsonFormatter = config.Formatters.JsonFormatter.SerializerSettings;
            WebApiConfig.Configure(config, movieProvider, loggingSource);
            Server = new HttpServer(config);
            _requestFactory = new RequestFactory(Server);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
                Server.Dispose();
            _disposed = true;
        }

        protected async Task<HttpResponse> ExecuteRequest(string uri, HttpMethod method)
        {
            return await ExecuteRequest(uri, method, null);
        }

        protected async Task<HttpResponse> ExecuteRequest(string uri, HttpMethod method, object content)
        {
            ConfigureController(MovieProviderMock.Object, LoggingSourceMock.Object);

            using (var request = _requestFactory.CreateRequest(uri, method))
            {
                if (content != null)
                {
                    request.Content = JsonConvert.SerializeObject(content);
                }

                return await request.ExecuteAsync();
            }
        }

        protected static void AssertStatusCode(HttpResponse response, HttpStatusCode statusCode)
        {
            response.StatusCode.Should().Be((int) statusCode);
        }

        protected void AssertExceptionLogged(Times times)
        {
            LoggingSourceMock.Verify(
                m => m.LogException(It.IsAny<Exception>(), It.IsAny<string>(), It.IsAny<object[]>()),
                times);
        }
    }
}
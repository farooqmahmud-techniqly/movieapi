﻿using System;
using System.Net.Http;
using System.Web.Http;
using Techniqly.RestEasy.Lib;

namespace Techniqly.Api.Movie.Tests
{
    internal sealed class RequestFactory
    {
        private readonly HttpServer _server;

        public RequestFactory(HttpServer server)
        {
            _server = server;
        }

        public Request CreateRequest(string uri, HttpMethod httpMethod)
        {
            if (httpMethod == HttpMethod.Get)
            {
                return new GetRequest(uri, _server);
            }

            if (httpMethod == HttpMethod.Post)
            {
                return new PostRequest(uri, _server);
            }

            throw new ArgumentException($"Invalid value for {nameof(httpMethod)} - {httpMethod}");
        }
    }
}
﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Techniqly.Api.Providers.Common.Movie;

namespace Techniqly.Api.Movie.Tests
{
    [TestFixture]
    internal class AddMovieTests : MovieApiTestsBase
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
        }

        private static readonly string Uri = $"{RootApiUri}/movies";

        [Test]
        public async Task Post_AddsMovie()
        {
            var movie = MovieFactory.CreateDefaultMovie();
            var result = new ProviderResult<Providers.Common.Movie.Models.Movie>(ProviderResultType.Added, movie);
            MovieProviderMock.Setup(m => m.AddMovie(It.IsAny<Providers.Common.Movie.Models.Movie>())).Returns(result);

            var response = await ExecuteRequest(Uri, HttpMethod.Post, movie);
            response.StatusCode.Should().Be((int) HttpStatusCode.Created);

            var rawResponse = response.RawResponse;
            rawResponse.Headers.Location.ToString().Should().Be($"{Uri}/{movie.Id}");
        }

        [Test]
        public async Task Post_WhenMovieNotSpecified_ReturnsBadRequest()
        {
            var response = await ExecuteRequest(Uri, HttpMethod.Post, null);
            response.StatusCode.Should().Be((int) HttpStatusCode.BadRequest);
            AssertExceptionLogged(Times.Once());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Techniqly.Api.Movie.Tests
{
    [TestFixture]
    internal class GetMovieTests : MovieApiTestsBase
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
        }

        private static readonly string Uri = $"{RootApiUri}/movies";

        [Test]
        public async Task Get_ReturnsMovieList()
        {
            var expectedMovies = new List<Providers.Common.Movie.Models.Movie>
            {
                MovieFactory.CreateDefaultMovie(),
                MovieFactory.CreateDefaultMovie()
            };

            MovieProviderMock.Setup(m => m.GetMovies()).Returns(expectedMovies);

            var response = await ExecuteRequest(Uri, HttpMethod.Get);
            AssertStatusCode(response, HttpStatusCode.OK);

            var actualMovies =
                JsonConvert.DeserializeObject<IEnumerable<Providers.Common.Movie.Models.Movie>>(response.Content,
                    JsonFormatter);
            actualMovies.Count().Should().Be(2);
        }

        [Test]
        public async Task Get_WhenMovieProviderThrowsException_LogsError()
        {
            MovieProviderMock.Setup(m => m.GetMovies()).Throws<InvalidOperationException>();

            var response = await ExecuteRequest(Uri, HttpMethod.Get);
            AssertStatusCode(response, HttpStatusCode.InternalServerError);
            AssertExceptionLogged(Times.Exactly(2));
        }

        [Test]
        public async Task Get_WhenNoMovies_ReturnsNotFound()
        {
            var response = await ExecuteRequest(Uri, HttpMethod.Get);
            AssertStatusCode(response, HttpStatusCode.NotFound);
            AssertExceptionLogged(Times.Once());
        }
    }
}
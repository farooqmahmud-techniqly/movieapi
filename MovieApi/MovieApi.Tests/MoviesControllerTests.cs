﻿using System;
using NUnit.Framework;
using Techniqly.Api.Movie.Controllers;

namespace Techniqly.Api.Movie.Tests
{
    [TestFixture]
    public class MoviesControllerTests
    {
        [Test]
        public void Constructor_WhenMovieProviderNull_ThrowsException()
        {
            Assert.Catch<ArgumentNullException>(() => new MoviesController(null));
        }
    }
}
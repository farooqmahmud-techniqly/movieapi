﻿using System.Web;
using System.Web.Http;
using Serilog;
using Techniqly.Api.Providers.Movie;
using Techniqly.MovieApi.Infrastructure;

namespace Techniqly.Api.Movie
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();
            WebApiConfig.Configure(GlobalConfiguration.Configuration, new MovieProvider(),
                new SerilogLoggingSource(logger));
        }
    }
}
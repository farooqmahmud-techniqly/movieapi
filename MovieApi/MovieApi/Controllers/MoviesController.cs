﻿using System;
using System.Linq;
using System.Web.Http;
using Techniqly.Api.Providers.Common.Movie;

namespace Techniqly.Api.Movie.Controllers
{
    [RoutePrefix("api/movies")]
    public class MoviesController : ApiController
    {
        private readonly IMovieProvider _movieProvider;

        public MoviesController(IMovieProvider movieProvider)
        {
            if (movieProvider == null)
            {
                throw new ArgumentNullException(nameof(movieProvider));
            }

            _movieProvider = movieProvider;
        }

        [HttpGet]
        [Route(Name = "GetMovies")]
        public IHttpActionResult Get()
        {
            var movies = _movieProvider.GetMovies().ToList();

            if (!movies.Any())
            {
                return NotFound();
            }

            return Ok(movies);
        }

        [HttpGet]
        [Route("{id}", Name = "GetMovie")]
        public IHttpActionResult Get(string id)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post(Providers.Common.Movie.Models.Movie movie)
        {
            if (movie == null)
            {
                return BadRequest();
            }

            var result = _movieProvider.AddMovie(movie);

            if (result.ResultType == ProviderResultType.Added)
            {
                return Created(Url.Link("GetMovie", new {id = result.Entity.Id}), result.Entity);
            }

            return InternalServerError();
        }
    }
}
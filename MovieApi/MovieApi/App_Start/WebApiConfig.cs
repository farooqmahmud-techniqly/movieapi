﻿using System.Reflection;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Autofac;
using Autofac.Integration.WebApi;
using MovieApi.Infrastructure.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Techniqly.Api.Movie.Logging;
using Techniqly.Api.Providers.Common.Movie;

namespace Techniqly.Api.Movie
{
    public static class WebApiConfig
    {
        public static void Configure(
            HttpConfiguration config,
            IMovieProvider movieProvider,
            ILoggingSource loggingSource)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional}
                );

            ConfigureFormatters(config);
            ConfigureAutofac(config, movieProvider, loggingSource);

            config.Services.Add(typeof(IExceptionLogger), new ApiExceptionLogger(loggingSource));
            config.MessageHandlers.Add(new ApiExceptionMessageHandler(loggingSource));
        }

        private static void ConfigureAutofac(
            HttpConfiguration config,
            IMovieProvider movieProvider,
            ILoggingSource loggingSource)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterInstance(movieProvider);
            builder.RegisterInstance(loggingSource);

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void ConfigureFormatters(HttpConfiguration config)
        {
            var settings = config.Formatters.JsonFormatter.SerializerSettings;

            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
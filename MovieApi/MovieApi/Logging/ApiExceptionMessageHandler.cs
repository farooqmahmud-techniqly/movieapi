﻿using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MovieApi.Infrastructure.Common;
using Techniqly.MovieApi.Infrastructure;

namespace Techniqly.Api.Movie.Logging
{
    internal sealed class ApiExceptionMessageHandler : DelegatingHandler
    {
        private readonly ILoggingSource _loggingSource;

        public ApiExceptionMessageHandler(ILoggingSource loggingSource)
        {
            _loggingSource = loggingSource;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                var message = await BuildExceptionMessage(request, response);
                var exception = new HttpRequestException(message);

                _loggingSource.LogException(exception, message);
            }

            return response;
        }

        private async Task<string> BuildExceptionMessage(HttpRequestMessage request, HttpResponseMessage response)
        {
            var requestErrorBuilder = new HttpRequestErrorLogBuilder(request);
            var requestErrorMessage = await requestErrorBuilder.Build();

            var responseErrorBuilder = new HttpResponseErrorLogBuilder(response);
            var responseErrorMessage = await responseErrorBuilder.Build();

            return new StringBuilder(requestErrorMessage).AppendLine(responseErrorMessage).ToString();
        }
    }
}
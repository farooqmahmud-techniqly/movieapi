﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using MovieApi.Infrastructure.Common;
using Techniqly.MovieApi.Infrastructure;

namespace Techniqly.Api.Movie.Logging
{
    internal sealed class ApiExceptionLogger : ExceptionLogger
    {
        private readonly ILoggingSource _loggingSource;

        public ApiExceptionLogger(ILoggingSource loggingSource)
        {
            _loggingSource = loggingSource;
        }

        public override async Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var message = await BuildUnhandledExceptionMessage(context);
            _loggingSource.LogException(context.Exception, message);
        }

        private async Task<string> BuildUnhandledExceptionMessage(ExceptionLoggerContext context)
        {
            var builder = new UnhandledExceptionLogBuilder(context.Exception, context.Request);
            return await builder.Build();
        }
    }
}
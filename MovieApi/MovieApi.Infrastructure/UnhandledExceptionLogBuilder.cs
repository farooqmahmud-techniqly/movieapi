﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Techniqly.MovieApi.Infrastructure
{
    public sealed class UnhandledExceptionLogBuilder
    {
        private readonly Exception _exception;
        private readonly HttpRequestMessage _request;

        public UnhandledExceptionLogBuilder(Exception exception, HttpRequestMessage request)
        {
            _exception = exception;
            _request = request;
        }

        public async Task<string> Build()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Unhandled exception processing {_request.Method} on {_request.RequestUri}");
            sb.AppendLine($"Exception: {_exception}");

            if (_request == null)
            {
                return sb.ToString();
            }

            sb.AppendLine($"Headers: {_request.Headers}");

            if (_request.Content == null)
            {
                return sb.ToString();
            }

            var content = await _request.Content.ReadAsStringAsync();
            sb.AppendLine($"Content: {content}");

            return sb.ToString();
        }
    }
}
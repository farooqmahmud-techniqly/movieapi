﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Techniqly.MovieApi.Infrastructure
{
    public sealed class HttpRequestErrorLogBuilder
    {
        private readonly HttpRequestMessage _request;

        public HttpRequestErrorLogBuilder(HttpRequestMessage request)
        {
            _request = request;
        }

        public async Task<string> Build()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Error processing message.");
            sb.AppendLine("Request details:");

            if (_request == null)
            {
                return sb.ToString();
            }

            sb.AppendLine($"Error processing {_request.Method} on {_request.RequestUri}");
            sb.AppendLine($"Request headers: {_request.Headers}");

            if (_request.Content == null)
            {
                return sb.ToString();
            }

            var contentBytes = await _request.Content.ReadAsByteArrayAsync();
            var content = Encoding.UTF8.GetString(contentBytes);
            sb.AppendLine($"Request content: {content}");

            return sb.ToString();
        }
    }
}
﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MovieApi.Infrastructure.Common;
using Serilog;

namespace Techniqly.MovieApi.Infrastructure
{
    public sealed class SerilogLoggingSource : ILoggingSource
    {
        private readonly ILogger _logger;

        public SerilogLoggingSource(ILogger logger)
        {
            _logger = logger;
        }

        public void LogException(Exception exception, string messageTemplate = null, params object[] values)
        {
            _logger.Error(exception, messageTemplate, values);
        }

        public Task LogExceptionAsync(Exception exception, CancellationToken cancellationToken,
            string messageTemplate = null,
            params object[] values)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Techniqly.MovieApi.Infrastructure
{
    public sealed class HttpResponseErrorLogBuilder
    {
        private readonly HttpResponseMessage _response;

        public HttpResponseErrorLogBuilder(HttpResponseMessage response)
        {
            _response = response;
        }

        public async Task<string> Build()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Error processing message.");
            sb.AppendLine("Response details:");

            if (_response == null)
            {
                return sb.ToString();
            }

            sb.AppendLine($"HTTP response error: {_response.StatusCode}");
            sb.AppendLine($"Response headers: {_response.Headers}");

            if (_response.Content == null)
            {
                return sb.ToString();
            }

            var contentBytes = await _response.Content.ReadAsByteArrayAsync();
            var content = Encoding.UTF8.GetString(contentBytes);
            sb.AppendLine($"Response content: {content}");

            return sb.ToString();
        }
    }
}
﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MovieApi.Infrastructure.Common
{
    public interface ILoggingSource
    {
        void LogException(Exception exception, string messageTemplate = null, params object[] values);

        Task LogExceptionAsync(Exception exception, CancellationToken cancellationToken, string messageTemplate = null,
            params object[] values);
    }
}
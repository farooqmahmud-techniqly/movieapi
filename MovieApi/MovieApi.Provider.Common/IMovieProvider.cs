﻿using System.Collections.Generic;

namespace Techniqly.Api.Providers.Common.Movie
{
    public interface IMovieProvider
    {
        IEnumerable<Models.Movie> GetMovies();
        ProviderResult<Models.Movie> AddMovie(Models.Movie movie);
    }
}
﻿namespace Techniqly.Api.Providers.Common.Movie
{
    public class ProviderResult<T> where T : class
    {
        public ProviderResult(ProviderResultType resultType, T entity)
        {
            ResultType = resultType;
            Entity = entity;
        }

        public ProviderResultType ResultType { get; private set; }

        public T Entity { get; private set; }
    }
}